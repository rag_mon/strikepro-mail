# Mailu Docker сервисы

## Установка
1. Копируем файл с переменными среды docker
	> `cp mailu-env-example mailu.env`
2. Прописываем конфиги в mailu.env
3. Разворачиваем сервисы
	> `docker-compose up -d`
